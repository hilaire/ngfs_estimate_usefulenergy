# NGFS Scenarios - Estimate Useful Energy

This script takes as input the downscaled final energy data by country and fuel and applies energy conversion factors to derive an estimate of total useful energy.

See Section 3.1.4 in the [NGFS Scenarios Technical Documentation](https://www.ngfs.net/sites/default/files/ngfs_climate_scenarios_technical_documentation__phase2_june2021.pdf) for more details.
